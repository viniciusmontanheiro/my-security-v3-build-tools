### Installation
This project requires [Ruby]

```sh
$ ruby build.rb <new-application-name> <new-package-name> 
```
#### Optional parameters
```sh
$ ruby build.rb <new-application-name> <new-package-name> <old-application-name> <old-package-name> 
```



#!/usr/bin/env ruby

$new_application_name = ARGV[0]
$new_package_name = ARGV[1]
$current_application_name = ARGV[2]
$current_package_name = ARGV[3]

$project_path = "./my-security-v3"

# VALIDATION

if $new_application_name.nil?
  puts "ERROR - The new application name was not informed!"
  abort
end

if $new_package_name.nil?
  puts "ERROR - the new package name was not informed!"
  abort
end

if $current_application_name.nil?
  $current_application_name = "My Security"
  puts "WARN - The current application name was not informed so we defined " + $current_application_name + " as default!"
end

if $current_package_name.nil?
  $current_package_name = "br.com.segware.mysecurity"
  puts "WARN - The current package name not informed so we defined " + $current_package_name + " as default!"
end

if !File.directory?($project_path)
  puts "ERROR - Project path not exists!"
  abort
end

# IMPORTS

require 'fileutils'
require 'rbconfig'

$operating_system = RbConfig::CONFIG['host_os']
$is_ios_os = false
$is_linux_os = false

if($operating_system.downcase.include?('darwin'))
  require('plist')
  $is_ios_os = true
elsif $operating_system.downcase.include?('linux')
  $is_linux_os = true;
end

# DEFINING FUNCTIONS

def remove_white_spaces(str)
  str.gsub(/\s+/, "")
end

def update_file(file_path, current_text, new_text)
  puts "Updating #{file_path}"
  
  if File.exist?(file_path)
    file = File.read(file_path)
    file_updated = file.gsub(current_text, new_text)
    File.open(file_path, "w") do |f|
      f.write(file_updated)
    end
  else
    puts "WARN - File " + file_path + " not exists! Please verify..."
  end
end


def update_ios_identifier(path)
  plist_path = path + "/" + $current_application_name + "/Info.plist"
  plist_file = Plist::parse_xml(plist_path)
  plist_file['CFBundleDisplayName'] = $new_application_name
  plist_file['CFBundleName'] = $new_application_name 
  plist_file['CFBundleIdentifier'] = $new_package_name
  File.open(plist_path, "w") do |f|
    f.write(plist_file.to_plist)
  end
end

def create_new_android_package_folders(path)
  java_path = path + "/app/src/main/java/"
  current_path = $current_package_name.gsub(".","/")
  current_package_path = java_path + current_path

  if !File.directory?(current_package_path)
    puts "ERROR - Directory of current package ->" + current_package_path + " not exists!"
    abort
  end 

  new_path = $new_package_name.gsub(".","/")
  new_package_path = java_path + new_path
  FileUtils::mkdir_p new_package_path

  if !File.directory?(new_package_path)
    puts "ERROR - Directory of new package ->" + new_package_path + " not exists!"
    abort
  end 

  java_files = Dir[current_package_path + "/*.java"]

  java_files.each do |file|
    update_file(file, $current_package_name, $new_package_name)
    if file.include? "MainActivity"
      update_file(file, "\"#{$current_application_name.gsub(/\s+/, "")}\"" , "\"#{$new_application_name.gsub(/\s+/, "")}\"")
    end
    FileUtils.cp(file, new_package_path)
  end
  FileUtils::rm_rf current_package_path
end

def update_android_identifier(path)
  create_new_android_package_folders(path)
  update_file(path + "/app/build.gradle", $current_package_name, $new_package_name)
  update_file(path + "/app/BUCK", $current_package_name, $new_package_name)
  update_file(path + "/app/src/main/AndroidManifest.xml", $current_package_name, $new_package_name)
  update_file(path + "/app/release/output.json", $current_package_name, $new_package_name)
  update_file(path + "/app/src/main/res/values/strings.xml", $current_application_name, $new_application_name)
  update_file(path + "/settings.gradle", $current_application_name, $new_application_name)
end

def generate_new_android_app
  if $is_linux_os || $is_ios_os
    puts "Starting to build a new Android application..."
    android_path = $project_path + "/android"
    update_android_identifier(android_path)
  else
    puts "ERROR - The operatings system " + $operating_system + " is not supported to build a Android application!"
  end
end 

def generate_new_ios_app
  if $is_ios_os
    puts "Starting to build a new IOS application..."
    ios_path = $project_path + "/ios"
    update_ios_identifier(ios_path)
  else
    puts "ERROR - The operating system " + $operating_system + " is not supported to build a IOS application"
  end
end

def execute_commons
  puts "Executing commons..."
  update_file($project_path + "/app.json", "\"#{$current_application_name}\"" , "\"#{$new_application_name}\"")
  update_file($project_path + "/index.js", $current_application_name.gsub(/\s+/, "") , $new_application_name.gsub(/\s+/, ""))
  update_file($project_path + "/package.json", $current_application_name.gsub(/\s+/, "") , $new_application_name.gsub(/\s+/, ""))
end

def start_build_process
  generate_new_android_app()
  generate_new_ios_app()
  execute_commons()
  puts "Build finished!"
end

start_build_process()

